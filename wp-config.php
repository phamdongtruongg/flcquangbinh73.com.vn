<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'MalibuMGMHoiAn');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=Xn#wd0X[6Gv+<-0QrvP(RaV?SJ#zHsgE$7lc_foG[vHRO>h05F1*Y)8h+N3t<@X');
define('SECURE_AUTH_KEY',  'tC}}Zuhok{!6:/G/aQSewsd`DfVyU~LRl!@unFfP}BgQ(`oh)8eZs.{Tn_%GZ2lD');
define('LOGGED_IN_KEY',    '9Ns=VY}K 3r/!-t?5|d-vYuW5 #)${nPSY,S=z{a_KYj9(+v94 f8xD1^+[8~ten');
define('NONCE_KEY',        'Nop>h-Q}(]yG)Gq/B#fs~/iFNh[rT<wS?.}hKioG?{A:$&5tf,(?$X{Cs^FdG%[B');
define('AUTH_SALT',        ';C9Z`7T0JL!4+ EUCcj@L(k,z94go9x->.,4p[cXs!tiZq.t25S&sZ&pDdm!lFyi');
define('SECURE_AUTH_SALT', '{6Ow=hD&KEABg`Kfk&qjB-q3U!E,Yqu%TIdms55>I4aCsB@I7<bzwM!Tb+$*v$Xj');
define('LOGGED_IN_SALT',   '6:nHf=|tHyd.p5udlA/TN||^@.nqy76eo<g-o&b>*6.S!mAt/_;-1QF|l?<?m]Jc');
define('NONCE_SALT',       'tOqssJ?7<{^0,J1^>GYJK1Ia,t<]0Fb6-N3vKp{7qu;G4?} qsF,anXUc*#P68mC');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
