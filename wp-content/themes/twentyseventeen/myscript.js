$(document).ready(function () {
    var scrollTop = 0;
    $(window).scroll(function () {
        scrollTop = $(window).scrollTop();
        if (scrollTop >= 1) {
            $('#global-nav').addClass('scrolled-nav');
        } else if (scrollTop < 1) {
            $('#global-nav').removeClass('scrolled-nav');
        }

    });

});