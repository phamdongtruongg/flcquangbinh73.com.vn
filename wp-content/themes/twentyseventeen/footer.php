<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>
<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js" integrity="sha384-kW+oWsYx3YpxvjtZjFXqazFpA7UP/MbiY4jvs+RWZo2+N94PFZ36T6TFkc9O3qoB" crossorigin="anonymous"></script>
<script>
	jQuery(document).ready(function ($) {
		$(document).ready(function () {
            var scrollTop = 0;
            if ( $( window ).width() > 480 ) {
                $(window).scroll(function () {
                scrollTop = $(window).scrollTop();
                if (scrollTop > 0) {
                    $('#global-nav').addClass('scrolled-nav');
                } else if (scrollTop == 0) {
                    $('#global-nav').removeClass('scrolled-nav');
                }
            });
            } else {
                $('#global-nav').css("position", "relative !important")
            }

            // GO TO TOP

            $("#gototop a").click(function() {
                $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            });
        });
        $(document).ready(function () {
            $('.elementor-element-eb9ad54 .watchMore').click(function() {
                $('.elementor-element-eb9ad54 h3').addClass('longerForH3');
                $('.elementor-element-eb9ad54 .watchMore').css('opacity', '0');
                $('.elementor-element-eb9ad54').css('padding-bottom', '50px' );
            })
            if ( $( window ).width() <= 480 ) {
                $( '.elementor-element-d7fb770' ).height( $( '.elementor-element-233fca4' ).height() * 2 );
            }
        });
    
    });

</script>
</body>
</html>
